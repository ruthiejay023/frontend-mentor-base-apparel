/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./public/**/*.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {},
    colors: {
      "desaturated-red": "hsl(0, 36%, 70%)",
      "soft-red": "hsl(0, 93%, 68%)",
      "dark-grayish-red": "hsl(0, 6%, 24%)",
      "white": "#fff",
      "light-pink": "hsla(0, 80%, 86%, 1)",
      'error': 'hsla(0, 96%, 61%, 1)',
      "peach": "hsla(0, 74%, 74%, 1)"
    },
    fontFamily: {
      JosefinSans: ['Josefin Sans', 'cursive'],
    }
  },
  plugins: [],
}
// hsla(0, 80%, 86%, 1), hsla(0, 74%, 74%, 1)